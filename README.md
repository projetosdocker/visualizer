# visualizer

> Isso funciona apenas com o Docker Swarm Mode no Docker Engine 1.12.0 e posterior. Ele não funciona com o projeto Docker Swarm separado

Contêiner de demonstração que exibe os serviços do Docker em execução em um Docker Swarm em um diagrama.

**Isso funciona apenas com o modo de enxame do Docker, que foi introduzido no Docker 1.12. Essas instruções presumem que você esteja executando no nó principal e já tenha um Swarm em execução.**

Cada nó no Swarm mostrará todas as tarefas em execução nele. Quando um serviço for desativado, ele será removido. Quando um nó desce, ele não irá; em vez disso, o círculo na parte superior ficará vermelho para indicar que ele desceu. As tarefas serão removidas. Ocasionalmente, a API remota retornará dados incompletos, por exemplo, o nó pode estar sem um nome. Na próxima vez que informações sobre esse nó forem obtidas, o nome será atualizado.

Para criar o serviço deve ser executado o seguinte comando:
```
docker stack deploy -c stack.yml visualizer
```

Para verificar o status do serviço, deve ser executado o seguinte comando:
```
docker service ps visualizer_visualizer
```

Para remove o serviço deve ser executado o seguinte comando:
```
docker service rm visualizer
```
